# Helloworld

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Steps to install nodejs in ubuntu server
curl -sL https://deb.nodesource.com/setup_16.x -o /tmp/nodesource_setup.sh
## you can confirm the configurations
nano /tmp/nodesource_setup.sh
## you can run the script 
sudo bash /tmp/nodesource_setup.sh
## now you can install the nodejs with apt package manager
sudo apt install nodejs
## check node version and confirm
node -v

## after installing node js now you can install the node package manager 
npm install

## build the the code
npm run build

## test the code 
npm run test

## after we run the build process, it will create dist folder. under this directory, we can see another folder called "helloworld"
for detailed information, we can see the docker file


### INSTALLATION OF  GITLAB RUNNER
sudo gitlab-runner register

## it will ask gitlab url and token
enter required things like https://gitlab.com  and token is available at project>settings>CI/CD>Click on expand at Runner. here we will get token.\ It will ask description for the runner.\ & it will ask "please enter the gitlab-ci tags for this runner (camma separated): enter where to run the project like stage, build, deploy, qa".\ press enter 2 times.\ it will ask "enter the executor:shell"

## Download the binary for your system
sudo curl -L --output /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64

## Give it permission to execute
sudo chmod +x /usr/local/bin/gitlab-runner

## Create a GitLab Runner user
sudo useradd gitlab-runner 

## Install and run as a service
sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
sudo gitlab-runner start
 
## Now register the gitlab-runner
sudo gitlab-runner register

